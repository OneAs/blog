<?php

namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;

class ApiResponse extends JsonResponse
{
    public static function error( $message = [] , $data  = [] )
    {
        \DB::rollback();
        $ret = [
            'status' => false ,
            'message' => $data ,
            'error' => $message
        ];

        return response()->json( $ret );
    }

    public static function validationError( $message = [] , $data  = [] )
    {
        \DB::rollback();
        $ret = [
            'status' => false ,
            'message' => $data ,
            'error' => [
                'code' => '400' , 
                'type' => 'BadRequest' , 
                'notices' => $message
            ]
        ];

        return response()->json( $ret );
    }

    public static function authError( $message = [] , $code = 401 , $data  = [] )
    {
        \DB::rollback();
        $ret = [
            'status' => false ,
            'message' => $data ,
            'error' => [
                'code' => $code , 
                'type' => 'Unauthorized' , 
                'notices' => $message
            ]
        ];

        return response()->json( $ret );
    }
    
    public static function notFoundError( $message = [] , $data  = [] )
    {
        \DB::rollback();
        $ret = [
            'status' => false ,
            'message' => $data ,
            'error' => [
                'code' => '404' , 
                'type' => 'NotFound' , 
                'notices' => $message
            ]
        ];

        return response()->json( $ret );
    }

    public static function success( $data = [] , $message = [] )
    {
        \DB::commit();
        $ret = [
            'status' => true ,
            'data' => $data
        ];

        if( count( $message ) > 0 )
            $ret['message'] = $message;

        return response()->json( $ret );
    }

    public static function debug( $obj )
    {
        \DB::rollback();
        $debugMsg = $errorMsg = $obj;
        if( is_a( $obj , "Illuminate\\Database\\QueryException" ) )
        {
            $debugMsg = $obj->getMessage();
            $errorMsg = $obj->errorInfo[2];
        }

        if( env( 'APP_DEBUG' ) )
        {
            return response()->json([
                'status' => 'error' ,
                'data' => [] ,
                'message' => $debugMsg ,
                'debug' => true ,
            ]);
        }
        else
        {
            return ApiResponse::error( $errorMsg );
        }
    }

    public static function dd()
    {
        return response()->json( func_get_args() );
    }
}
