<?php

namespace App\Http\Controllers;

use App\Models\Course;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use SebastianBergmann\Environment\Console;

class CourseController extends BaseController
{
    public function getAll()
    {
        $item = Course::all();
        return $item;
    }
    public function getCourseDetail(Request $request, $id)
    {
        $item = Course::find($id);

        if (is_null($item)) {
            return ApiResponse::error('missing order id : ' . $id);
        }

        return ApiResponse::success($item);
    }

    // public function createCourse(Request $request)
    // {
    //     $item = new Course();
    //     $item->id_course = $request->id_course;
    //     $item->name_course = $request->name_course;
    //     $item->service_code  = $request->service_code;;
    //     if ($item->save()) {
    //         return ApiResponse::success($item);
    //     }
    // }
    public function createCourse(Request $request)
    {


        $item = new Course();
        $item->id_course = $request->id_course;
        $item->name_course = $request->name_course;
        $item->service_code = $request->service_code;
        $item->brance = $request->brance;
        $item->nursing_level = $request->nursing_level;
        $item->professional = $request->professional;
        $item->type = $request->type;
        $item->category = $request->category;
        $item->content = $request->content;
        $item->budget = $request->budget;
        $item->income = $request->income;
        $item->organization = $request->organization;
        $item->bounty = $request->bounty;
        $item->dev_knowledge = $request->dev_knowledge;
        $item->dev_ocsc = $request->dev_ocsc;
        $item->dev_manage = $request->dev_manage;
        $item->dev_ops = $request->dev_ops;
        $item->dev_opp = $request->dev_opp;
        $item->logic = $request->logic;
        $item->object = $request->object;
        $item->property = $request->property;
        $item->quantity = $request->quantity;
        // $item->date_course = $request->date_course;
        $item->location_course = $request->location_course;
        $item->manage_course = $request->manage_course;
        $item->content_course = $request->content_course;
        $item->evaluate_course = $request->evaluate_course;
        $item->diploma = $request->diploma;
        // $item->lecturer = $request->lecturer;
        $item->statute = $request->statute;
        $item->info = $request->info;
        $item->file = $request->file;
        if ($item->save()) {
            return ApiResponse::success($item);
        } else {
            return    ApiResponse::error('Cannot Register');
        }
    }
}
