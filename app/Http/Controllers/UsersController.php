<?php

namespace App\Http\Controllers;

use App\Users;
use App\Models\Order;
use Firebase\JWT\JWT;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Laravel\Lumen\Routing\Controller as BaseController;
use App\Http\Middleware\CorsMiddleware;
use App\Models\Course;

class UsersController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function testjoin()
    {
        // $data = Users::find(1);
        // $data->course();
        // return ApiResponse::success($data);
        $data = Order::all();
        return ApiResponse::success($data);
    }

    public function getAll()
    {
        $data = Users::all();
        return ApiResponse::success($data);
        // return $this->handle($data);
        // echo "666";
    }

    public function regisCoures()
    {
        $data = Users::all();
        return ApiResponse::success($data);
        // return $this->handle($data);
        // echo "666";
    }
    public function getUser()
    {
        $data = Users::all();
        $data->load(['orders']);

        return ApiResponse::success($data);
        // return $this->handle($data);
        // echo "666";
    }
    public function getUserDetail(Request $request, $id)
    {
        $data = Users::find($id);

        if (is_null($data)) {
            return ApiResponse::error('missing order id : ' . $id);
        }

        return ApiResponse::success($data);
    }


    /*
    |--------------------------------------------------------------------------
    | Api สมัครสมาชิก
    |--------------------------------------------------------------------------
     */
    public function register(Request $request)
    {
        // validator
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|unique:users',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();

            return    ApiResponse::error($errors);
        } else {
            $user = new Users();
            $user->email = $request->email;
            $user->password = Hash::make($request->password);
            $user->taxid = $request->taxid;
            $user->name = $request->name;
            $user->lastname = $request->lastname;
            $user->nickname = $request->nickname;
            $user->type_user = $request->type_user;
            $user->birthday = $request->birthday;
            $user->phone = $request->phone;
            $user->line = $request->line;
            $user->pos = $request->pos;
            $user->pos_level = $request->pos_level;
            $user->pos_date = $request->pos_date;
            $user->pos_manage = $request->pos_manage;
            $user->add_work = $request->add_work;
            $user->add_work_t = $request->add_work_t;
            $user->add_work_a = $request->add_work_a;
            $user->add_work_p = $request->add_work_p;
            $user->add_home = $request->add_home;
            $user->add_home_t = $request->add_home_t;
            $user->add_home_a = $request->add_home_a;
            $user->add_home_p = $request->add_home_p;

            if ($user->save()) {
                $token = $this->jwt($user);
                $user['api_token'] = $token;
                return ApiResponse::success($user);
            } else {
                return    ApiResponse::error('Cannot Register');
            }
        }
    }

    /*
    |--------------------------------------------------------------------------
    | Api เข้าสู่ระบบ
    |--------------------------------------------------------------------------
     */
    public function login(Request $request)
    {

        $user = Users::where('email', $request->email)
            ->first();

        if (!empty($user) && Hash::check($request->password, $user->password)) {
            $token = $this->jwt($user);
            $user["api_token"] = $token;

            return ApiResponse::success($user);
        } else {
            return    ApiResponse::error("email or password is incorrect");
        }
    }

    /*
    |--------------------------------------------------------------------------
    | ตัวเข้ารหัส JWT
    |--------------------------------------------------------------------------
     */
    protected function jwt($user)
    {
        $payload = [
            'iss' => "lumen-jwt", // Issuer of the token
            'sub' => $user->id, // Subject of the token
            'name' => $user->name, // Subject of the token
            'lastname' => $user->lastname, // Subject of the token
            'iat' => time(), // Time when JWT was issued.
            'role' => $user->role,
            'exp' => time() + env('JWT_EXPIRE_HOUR') * 60 * 60, // Expiration time
            'payload'   => [
                'email'
            ]
        ];

        return JWT::encode($payload, env('JWT_SECRET'));
    }

    /*
    |--------------------------------------------------------------------------
    | response เมื่อข้อมูลส่งถูกต้อง
    |--------------------------------------------------------------------------
     */
    protected function responseRequestSuccess($ret)
    {
        return response()->json(['status' => 'success', 'data' => $ret], 200)
            ->header('Access-Control-Allow-Origin', '*')
            ->header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
    }

    /*
    |--------------------------------------------------------------------------
    | response เมื่อข้อมูลมีการผิดพลาด
    |--------------------------------------------------------------------------
     */
    protected function responseRequestError($message = 'Bad request', $statusCode = 200)
    {
        return response()->json(['status' => 'error', 'error' => $message], $statusCode)
            ->header('Access-Control-Allow-Origin', '*')
            ->header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
    }
}
