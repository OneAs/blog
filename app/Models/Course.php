<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{

    public function users()
    // {
    //     return $this->belongsToMany(\App\Users::class, 'order_user');
    // }
    {
        return $this->belongsTo(Users::class, 'user_id');
    }
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'course';
    protected $fillable = [
        'id', 'id_course', 'name_course', 'service_code', 'brance', 'nursing_level', 'professional', 'type', 'category', 'content', 'budget', 'income', 'organization', 'bounty', 'dev_knowledge', 'dev_ocsc', 'dev_manage', 'dev_ops', 'dev_opp', 'logic', 'object', 'property', 'quantity', 'date_course', 'location_course', 'manage_course', 'content_course', 'evaluate_course', 'diploma', 'lecturer', 'statute', 'info', 'file',
    ];
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    // protected $hidden = ['password'];
}
