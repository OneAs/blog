<?php

namespace App\Models;

// use App\Users;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    public function users()
    {
        return $this->belongsTo(Users::class, 'user_id');
    }


    protected $table = 'order_course';
    protected $fillable = [
        'user_id', 'cours_id', 'status_user', 'status_payment', 'status_learn', 'info'
    ];
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
}
