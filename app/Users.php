<?php

namespace App;

use App\Models\Order;
use Illuminate\Database\Eloquent\Model;

class Users extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    // public function course()
    // {
    //     return $this->belongsToMany(\App\Models\Course::class, 'user_id');
    // }
    public function orders()
    {
        return $this->hasMany(Order::class, 'user_id');
    }
    protected $table = 'users';
    protected $fillable = [
        'id', 'name', 'lastname', 'nickname', 'username', 'email', 'type_user', 'role', 'age', 'birthday', 'taxid', 'phone', 'line', 'pos', 'pos_level', 'pos_date', 'pos_manage', 'add_work', 'add_work_t', 'add_work_a', 'add_work_p', 'add_home', 'add_home_t', 'add_home_a', 'add_home_p',
    ];
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password'];
}
