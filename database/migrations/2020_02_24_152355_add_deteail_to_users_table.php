<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDeteailToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('lastname');
            $table->string('nickname');
            $table->integer('age');
            $table->string('birthday');
            $table->integer('taxid');
            $table->integer('phone');
            $table->string('line');
            $table->string('pos');
            $table->string('pos_level');
            $table->integer('pos_date');
            $table->string('pos_manage');
            $table->string('add_work');
            $table->string('add_work_t');
            $table->string('add_work_a');
            $table->string('add_work_p');
            $table->string('add_home');
            $table->string('add_home_t');
            $table->string('add_home_a');
            $table->string('add_home_p');
            //
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            Schema::dropIfExists('users');
        });
    }
}
