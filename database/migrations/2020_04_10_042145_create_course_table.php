<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCourseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('course', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('id_course')->unique();
            $table->string('name_course');
            $table->string('service_code');
            $table->string('brance');
            $table->string('nursing_level');
            $table->string('professional');
            $table->string('type');
            $table->string('category');
            $table->string('content');
            $table->integer('budget');
            $table->integer('income');
            $table->string('organization');
            $table->integer('bounty');
            $table->string('dev_knowledge');
            $table->string('dev_ocsc');
            $table->string('dev_manage');
            $table->string('dev_ops');
            $table->string('dev_opp');
            $table->string('logic');
            $table->string('object');
            $table->string('property');
            $table->integer('quantity');
            $table->date('date_course');
            $table->string('location_course');
            $table->string('manage_course');
            $table->string('content_course');
            $table->string('evaluate_course');
            $table->string('diploma');
            $table->string('statute');
            $table->string('info');
            $table->string('file');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('course');
    }
}
