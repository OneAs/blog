<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->options('{all:.*}', function () {
    return 'OK';
});
// ,'middleware' => ['cors']
$router->group(['prefix' => 'api/v1'], function ($router) {
    // Users
    // $router->get('getAll', 'UsersController@getAll');
    $router->post('user/register', 'UsersController@register');
    $router->get('testjoin', 'UsersController@testjoin');

    $router->post('user/login', ['uses' => 'UsersController@login']);
    $router->get('userdetail/{id:[0-9]+}', 'UsersController@getUserDetail');

    $router->get('test1', 'UsersController@getAll');
    $router->get('test2/{id:[0-9]+}', 'UsersController@getOrderDetail');
    $router->get('course', 'CourseController@getAll');
    $router->post('create', 'CourseController@createCourse');
    $router->get('coursedetail/{id:[0-9]+}', 'CourseController@getCourseDetail');
});

$router->group(['prefix' => 'api/v1', 'middleware' => 'jwt.auth:admin'], function ($router) {
    $router->get('test', 'UsersController@getAll');
});


// $router->group(['prefix' => 'api/v1' ], function () use ($router) {
//     $router->get('getAll', 'UsersController@getAll');
//     $router->get('getID/{id}', 'UsersController@getID');
//     $router->post('insertData', 'UsersController@addData');
//     $router->put('updateData/{id}', 'UsersController@updateData');
//     $router->delete('deleteData/{id}', 'UsersController@deleteData');
//     $router->post('user/login', ['uses' => 'UsersController@login']);
// });

// Route::get('check-connect',function(){
//     if(DB::connection()->getDatabaseName())
//     {
//     return "Yes! successfully connected to the DB: " . DB::connection()->getDatabaseName();
//     }else{
//     return 'Connection False !!';
//     }
   
//    });
